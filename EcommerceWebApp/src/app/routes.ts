
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './modules/home/home.component';
import { RegisterComponent } from './modules/register/register.component';
import { LoginComponent } from './_components/login/login.component';
import { NavbarComponent } from './_components/shared/common/navbar/navbar.component';
import { IntroductionComponent } from './modules/introduction/introduction.component';
import { AuthGuard } from './_guards/auth.guard';
// import { EmployeeComponent } from './pages/employee/employee/employee.component';

const appRoutes: Routes = [
  { path: 'home', component: HomeComponent },
  // {
  //   path: 'in-out-gate',
  //   canActivate: [AuthGuard],
  //   component: InOutGateComponent,
  // },
  // {
  //   path: 'vehicle-card',
  //   canActivate: [AuthGuard],
  //   component: VehicleCardComponent,
  // },
  // {
  //   path: 'problem-list',
  //   canActivate: [AuthGuard],
  //   component: ProblemListComponent,
  // },
  // {
  //   path: 'price-listaa',
  //   canActivate: [AuthGuard],
  //   component: PriceListComponent,
  // },
  // {
  //   path: 'resolve-record',
  //   canActivate: [AuthGuard],
  //   component: ResolveRecordComponent,
  // },
  // { path: '', component: InOutGateComponent, canActivate: [AuthGuard] },
  //otherwise redirect to home
  // { path: '', component: NavbarComponent },

  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'login', component: LoginComponent },
  { path: 'register', component: RegisterComponent },
  { path: 'introduction', component: IntroductionComponent },
];

export const routing = RouterModule.forRoot(appRoutes);
