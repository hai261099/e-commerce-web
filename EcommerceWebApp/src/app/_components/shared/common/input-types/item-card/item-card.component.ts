import { Item } from './../../../../../_models/item';
import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'item-card',
  templateUrl: './item-card.component.html',
  styleUrls: ['./item-card.component.css']
})
export class ItemCardComponent implements OnInit {
  @Input() item : Item;
  constructor() { }

  ngOnInit() {
  }

}
