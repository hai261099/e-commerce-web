
export interface Item {
  id: number;
  itemName: string;
  cost: number;
  photoUrl?: string;
}
