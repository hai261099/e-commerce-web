import { Item } from './../../_models/item';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { first } from 'rxjs/operators';
// import { Employee } from 'src/app/_models/employee';
import { AlertService } from '../../_services/alert.service';
import { AuthenticationService } from '../../_services/authentication.service';
declare let alertify: any;
@Component({
  selector: 'home',
  templateUrl: 'home.component.html',
  styleUrls: ['home.component.scss'],
})
export class HomeComponent implements OnInit {
  items : Item[];
  test: Item;
  loading = false;
  submitted = false;
  returnUrl: string;
  userName;
  password;
  firstName: string;
  arr = [{name:'123',cost:123,id:123}]

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private authenticationService: AuthenticationService,
    private alertService: AlertService
  ) {
    // if (this.authenticationService.currentUserValue) {
    //   this.router.navigate(['/']);
    // }
  }

  ngOnInit() {
    // this.test.id = 1;
    // this.test.itemName = 'abc';
    // this.test.cost = 123;
    // this.test.photoUrl = null;
     this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
    //  this.items.push(this.test)
  }

  onSubmit() {
    this.submitted = true;

    this.loading = true;

    // var user = new Employee();
    // user.Username = this.userName;
    // user.Password = this.password;

    // this.authenticationService
    //   .login(user)
    //   .pipe(first())
    //   .subscribe(
    //     (data) => {
    //       if (data === null) return alertify.error('Tài khoản hoặc mật khẩu không chính xác');
    //       this.router.navigate([this.returnUrl]);
    //       this.alertService.success('Success');
    //       alertify.success('Đăng nhập thành công');
    //     },
    //     (error) => {
    //       this.alertService.error(error);
    //       this.loading = false;
    //     }
    //   );
  }
}
